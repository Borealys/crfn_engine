CRFN - The Role Playing Game Engine
===================================

This game engine was fistrly inspired by the need to build a specific one for
my universe.

Then, the idea rise up to create a more global game engine which can ben used
for any other game of the same kind.

Right now, I am just looking for the global structure of the database and the
code.
So for now, this file will only contain ideas of implementation.

Building/install instruction and documentation will come in when the project
will become mature.

Functionalities
---------------

Here we are building an engine for turn by turn games.

The main rules are:
* The ability to build a team with different roles for people
* Main caracteristics and skills for them
* Kind of objects

Database Model
--------------

### attributes

This table may hold main caracteristics, passive and active skills.
The main columns will be:
* name
* description
* duration (in number of turn. 0 for never finish, 1 for immediate skills).
* action (This one will hold instructions aimed to be interpreted by a specific
interpreter which will be developed later).

### families

The family of any attributes:
* name
* description

### attributes_families

n-n link between attributes and families.

### jobs

A table to hold any jobs a character may do.
The minimum consist in 'name' and 'description'.

We may add access condition (TODO: how?)

### attributes_jobs

Some attributes may be limited to some jobs.
* attr\_id
* job\_id
* level (to indicate a minimum level in the job to get the access to the
attribute

A family relationship can be added to add inheritence between two types.

### types

A table to hold all elements (inanimate and animate) of the game.
Minimum fields are:
* name
* description

### attribut_target

Some attributes may only targets some kind of types (by default, it may target
anything).

### attribut_dependencies

Some attributes may want a minimum level of knowledge in one or many other(s)
attributes:
* attribute\_id
* dependency\_id
* level

### scenarii

Hold the metas of adventures:
* name
* description
* minimum\_level
* minimum\_team\_size
* first\_chapter\_id

### chapters

All the chapters for a scenarii:
* name
* kind (something like 'battle', 'history', etc.)
* text
* event (to launch some event like a battle)

A n-n table 'next\_chapters' can be used to link a chapter to the next ones.
(Multiple because we can follow various ways). 

### characters

To hold all characters of the game:
* name
* description
* is\_leader (usefull for team of charcters under the control of the player)
* cost (0 for leaders. For others, it may be usefull to refine the team
abilities)

A 1-n table 'team' will be available to link a leader to its team.

A 'character\_type' will be usefull to hold the type(s) of a character (elf,
human, dwarf, ...).

Object model
------------

### Scenario

To simplify, any adventure is composed with scenarii which is a set of
chapters.

The end of each chapter result in a choice (explicit or implicit) where the
player will have to choose the next chapter.
Implicit may occure (for instance) when a battle takes place in the chapter.
The next chapter may depend on the result of the battle. (For example, defeat
leads the player to a jail, victory to the treasure).

So, there is two main classes:
* Scenario which reflect the table in database
* Chapter, which do the same.

### Actions

There are 4 main classes:
* Character which will used to represent actors of the action
* ActionSet which holds actions that a character will have to do
* ActionMaker which implement the "Role Player Action" language
* ActionContext which hold all environmental info like the turn number, the
action owner, the targets, ... And any information which can affect the action.

#### Character

Only one publi method 'do' which take an action in parameter.
Private methods will do checks to know if the character can do the action,
and so other actions to define.

The main attribute of this class will be the "character" from database.

Some attributes may represent calculated attributes of the character from its
database attributes.
For example, life points are calculated from stamina, power from strength, etc.

#### ActionSet

Basically, its an array of actions with some specific methods (to define).
The actions will be unstacked to the "do" method of character until its action
points are spent.

#### ActionMaker

This is basically an interpreter which translate attributes actions in python.
Because we don't have hard thing to do, the main structure can be fixed to a
basic form like "verb" "subject" "operation".

Verbs can be:
* do: to execute an action
* define: to define local values (i.e. in the scope of the action)
* ... (TODO)

Subject will be a var of the environment (target.stamina, player.strength,
...).

Operations will be the action to do or mathematical operators.
For instance, we can have

    do target.stamina - player.strength.

Then, operations can be attributes called from database.
In this case, the action environment, action owner and targets must be
provided.

To improve flexibility, conditional operators can be implemented as a simple
"if", "elif", "fi" suite.

For long time operations (like enchants, ritual, etc.), loops will be
implemented:
* for <n> turns ... end
* until <condition> ... end

Then, python coroutines mechanism can be used to call the loop content at each
turn of the game.

Note: not sure that's a good idea. We must be able to stop the game and come
back later (save/download the state of the game).

#### ActionContext

This class has to hold as many information as possible, which can be useful for
the action, from the environment.
In the absolute, more information in the context means more elements which can
be used to build custom action, and so custom skills for characters.